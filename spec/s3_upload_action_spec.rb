describe Fastlane::Actions::S3UploadAction do
  describe '#run' do
    it 'prints a message' do
      expect(Fastlane::UI).to receive(:message).with("The s3_upload plugin is working!")

      Fastlane::Actions::S3UploadAction.run(nil)
    end
  end
end
