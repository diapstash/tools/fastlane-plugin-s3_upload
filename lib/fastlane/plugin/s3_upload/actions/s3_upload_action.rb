require 'fastlane/action'
require_relative '../helper/s3_upload_helper'
require 'aws-sdk-s3'

module Fastlane
  module Actions
    class S3UploadAction < Action
      def self.run(params)

        client = Aws::S3::Client.new(
          region: params[:region],
          credentials: Aws::Credentials.new(params[:access_key], params[:secret_key]),
          endpoint: params[:endpoint],
          force_path_style: params[:force_path_style]
        )
        resource = Aws::S3::Resource.new(client: client)
        bucket = resource.bucket(params[:bucket])
        if File.exist?(params[:inputFile])
          object_key = File.join(params[:destPath], File.basename(params[:inputFile]))
          UI.message("#{params[:inputFile]} uploading to #{params[:bucket]}:#{object_key}...")
          bucket.object(object_key).upload_file(params[:inputFile])
          UI.success("#{params[:inputFile]} uploaded")
          true
        else
          UI.user_error!("File #{params[:inputFile]} not found")
          false
        end
      rescue StandardError
        UI.user_error!("Error when uploading #{params[:inputFile]} not found")
        false
      end


      def self.description
        "Upload file to S3 bucket"
      end

      def self.authors
        ["Rantigr"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "Upload file to S3 bucket"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :inputFile,
                                       env_name: "S3_INPUT_FILE",
                                       description: "File to upload",
                                       optional: false,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :destPath,
                                       env_name: "S3_DEST_PATH",
                                       description: "Parent folder path in S3 bucket",
                                       optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :access_key,
                                       env_name: "S3_ACCESS_KEY",
                                       description: "S3 Access key",
                                       optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :secret_key,
                                       env_name: "S3_SECRET_KEY",
                                       description: "S3 Secret key",
                                       optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :bucket,
                                       env_name: "S3_BUCKET",
                                       description: "S3 Bucket",
                                       optional: false,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :region,
                                       env_name: "S3_REGION",
                                       description: "S3 region",
                                       optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :endpoint,
                                       env_name: "S3_ENDPOINT",
                                       description: "S3 endpoint",
                                       optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :force_path_style,
                                       env_name: "S3_FORCE_PATH_STYLE",
                                       description: "S3 force path style (ex: minio)",
                                       optional: true,
                                       default_value: false,
                                       type: Boolean),


        ]
      end

      def self.is_supported?(platform)
        # Adjust this if your plugin only works for a particular platform (iOS vs. Android, for example)
        # See: https://docs.fastlane.tools/advanced/#control-configuration-by-lane-and-by-platform
        #
        # [:ios, :mac, :android].include?(platform)
        true
      end
    end
  end
end
